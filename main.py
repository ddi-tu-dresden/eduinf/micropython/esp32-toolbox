import toolbox

# i1 = connectInput(1)
# i2 = connectInput(2)
# i3 = connectInput(3)
# i4 = connectInput(4)
# i5 = connectInput(5)

# o1 = connectOutput(1)
# o2 = connectOutput(2)
# o3 = connectOutput(3)
# o4 = connectOutput(4)
# o5 = connectOutput(5)

# o1.value(1)
# o2.value(1)
# o3.value(1)
# o4.value(1)
# o5.value(1)

# connectServo(1)
# setAngle(20)

roteLED = connectOutput(1)
gelbeLED = connectOutput(2)
pause = 1
anderreihe = roteLED

while True:
    if anderreihe == roteLED:
        roteLED.value(True)
        sleep(pause)
        roteLED.value(False)
        anderreihe = gelbeLED
    else:
        gelbeLED.value(1)
        sleep(pause)
        gelbeLED.value(0)
        anderreihe = roteLED
